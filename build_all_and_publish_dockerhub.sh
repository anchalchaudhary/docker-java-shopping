#!/bin/bash

cd shopfront
mvn clean install
if docker build -t anchalchaudhary/shopfront . ; then
  docker push anchalchaudhary/shopfront
fi
cd ..

cd productcatalogue
mvn clean install
if docker build -t anchalchaudhary/productcatalogue . ; then
  docker push anchalchaudhary/productcatalogue
fi
cd ..

cd stockmanager
mvn clean install
if docker build -t anchalchaudhary/stockmanager . ; then
  docker push anchalchaudhary/stockmanager
fi
cd ..
